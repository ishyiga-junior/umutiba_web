getData();
async function getData() {
  const response = await fetch(
    "https://umutiba-api.herokuapp.com/products/Kigali"
  );
  const apiData = await response.json();
  length = apiData.data.length;
  labels = [];
  values = [];
  for (i = 0; i < length; i++) {
    labels.push(apiData.data[i].name);
    values.push(apiData.data[i].quantity);
  }
  values[i] = 0;
  new Chart(document.getElementById("bar-chart"), {
    type: "bar",
    data: {
      labels: labels,
      datasets: [
        {
          label: "Qantity per Product",
          backgroundColor: [
            "#3e95cd",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#40E0D0",
            "#000080",
            "#008080",
            "#800080",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#40E0D0",
            "#000080",
            "#008080",
            "#800080",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#3e95cd",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#40E0D0",
            "#000080",
            "#008080",
            "#800080",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
            "#40E0D0",
            "#000080",
            "#008080",
            "#800080",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850",
            "#CD5C5C",
          ],
          data: values,
        },
      ],
    },
    options: {
      legend: { display: true },
      title: {
        display: true,
        text: "Umutiba Products",
      },
    },
  });
}
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}
// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches(".dropbtn")) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  }
};
function fromPaccy() {
  const api_url = "https://umutiba-api.herokuapp.com/users/all";
  async function getusers() {
    const response = await fetch(api_url);
    const data = await response.json();
    const { total } = data;
    document.getElementById("uuu").textContent = total;
  }
  getusers();
}
function loadAndRenderData(product) {
  let template = document.querySelector("tbody");
  //console.log(fetchData());
  city = product.location.cityName;
  address = product.location.cityName;
  // var i=1;

  //     console.log("This is the product "+(i),product);
  //         i++;
  let row = `
                <td>${product.niki_code}</td>
                <td>${product.name}</td>
                <td>${product.full_name}</td>
                <td>${product.mobile_no}</td>
                <td>${product.location.cityName}</td>
                <td>${product.price}</td>
                <td>${product.quantity}</td>
                <td><image src="${product.image}" style="width:50px; height:50px" onclick="window.open(this.src)"/></td>
                <td>${product.added_on}</td>
                <td><input type="checkbox" id="" value="xx"/></td>
                <td onclick="linkFunction()"><input type="button" name="info" value="more Info"/></td>
                                          
          `;
  let rw = `
          <td>${product.niki_code}</td>
          <td>${
            product.name
          }</td>                                                                                 
          <td>${
            "Latitude:" +
            product.location.latitude +
            "<br>" +
            "Longitude:" +
            product.location.longitude
          }</td>
          <td>${product.price}</td>
          <td><image src="${
            product.image
          }" style="width:50px; height:50px" onclick="window.open(this.src)"/></td>
          <td>${product.added_on}</td>
          <td><input type="checkbox" id="" value="xx"/></td>
          <td onclick="linkFunction()"><input type="button" name="info" value="more Info"/></td>
                                    
    `;
  // t = t.replace(/{value6}/g, product.value4);
  let tr = document.createElement("tr");
  if (city == null) {
    tr.innerHTML = rw;
  } else {
    tr.innerHTML = row;
  }

  template.appendChild(tr);

  // console.log(jsonData);
}
//fetchdata
function fetchData() {
  if (localStorage.getItem("token") == null) {
    window.open("home.html", "_self");
  }
  var products;
  fetch("https://umutiba-api.herokuapp.com/products/all").then((response) =>
    response
      .json()
      .then((data) => {
        products = data.products;
        products.forEach((product) => {
          loadAndRenderData(product);
        });
      })
      .then(() => {
        $(document).ready(function () {
          $("#example").DataTable({
            dom: "Bfrtip",
            buttons: ["copy", "csv", "excel", "pdf", "print"],
          });
        });
      })
  );
}
function linkFunction(x) {
  document.location.href = "itemInfo.html";
}

function hideDiv() {
  var coll = document.getElementsByClassName("collapsible");
  var i;
  for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.maxHeight) {
        content.style.maxHeight = null;
      } else {
        content.style.maxHeight = content.scrollHeight + "px";
      }
    });
  }
}
//function for counting quantities
function countStock() {
  const api_url = "https://umutiba-api.herokuapp.com/products/all";
  async function getproducts() {
    const response = await fetch(api_url);
    const data = await response.json();
    const { products } = data;
    sum = 0;
    for (i = 0; i < products.length; i++) {
      sum = !isNaN(products[i]["quantity"])
        ? sum + products[i]["quantity"]
        : sum;
    }
    document.getElementById("aaa").textContent = sum;
  }
  getproducts();
}
//function for categorizing items in stock
function countCategory() {
  const apii = "https://umutiba-api.herokuapp.com/products/all";
  async function getcategories() {
    const response = await fetch(apii);
    const data = await response.json();
    const { products } = data;
    let n = products.length;
    cats = [];
    for (i = 0; i < n; i++) {
      for (j = i + 1; j < n; j++) {
        //console.log(products[i]['name']);
        if (products[i]["name"] == products[j]["name"]) {
          while (j < n) {
            products[j]["name"] = products[j - 1]["name"];
            cats[i] = products[i].name;
            j++;
          }
          --n;
        } else {
          continue;
        }
      }
    }
    document.getElementById("cat").textContent = n;
  }
  getcategories();
}
